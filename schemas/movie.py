from pydantic import BaseModel, Field
from typing import Optional, List


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=15)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2022)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=1, max_length=20)


# valore por defect

class Config:
    schema_extra = {
        "example":    {
            "id": 1,
            "title": "Peli",
            "overview": "En seres que ...",
            "year": 2022,
            "rating": 7.8,
            "category": "Acción"
        }
    }
